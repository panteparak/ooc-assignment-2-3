List of Commands:
info - list player infos
take - pick up item in the current room
drop {ItemName} - drop player's item in the current room (if it's an empty room)
use {PotionName} - Use selected potion
go {Direction} - go the the the specify direction
attack-with {weapon} - attack monster with weapon
help - prints out help
quit - Quit the game
look - prints out what's in the current room

To Run the game:
mvn package
java -jar target/TheGame.jar

Game:
To Edit the levels, Go to GameController Class and implement a LevelGenerator nested around LevelConfigurator interface
If invalid values is put in the config, exception will be raise (InvalidLevelConfigurationException) see error message for details