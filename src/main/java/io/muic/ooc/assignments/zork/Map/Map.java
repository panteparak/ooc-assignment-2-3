package io.muic.ooc.assignments.zork.Map;

import io.muic.ooc.assignments.zork.Map.LevelGenerator.LevelGenerator;
import io.muic.ooc.assignments.zork.Characters.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Map {
    private List<LevelGenerator> lstMaps;
    private int currentLevel = 0;
    private Player player = null;
    private Point currentLocation = null;
    private LevelGenerator level = null;

    public Point getCurrentLocation() {
        return currentLocation;
    }

    public LevelGenerator getLevel() {
        return level;
    }

    public Map() {
        this.lstMaps = new ArrayList<>();
        this.player = new Player();
    }

    public void addLevel(LevelGenerator level){
        this.lstMaps.add(level);
    }

    public void addAllLevels(LevelGenerator... levels){
        this.lstMaps.addAll(Arrays.asList(levels));
    }

    public LevelGenerator removeLevel(int index){
        return this.lstMaps.remove(index);
    }

    public boolean removeLevel(LevelGenerator level){
        return this.lstMaps.remove(level);
    }

    public int goToNextLevel(){
        if (this.currentLevel < this.lstMaps.size() - 1){
            this.currentLevel += 1;
        } else {
            System.out.println("Hana: You beated the game, GoodBye!");
            System.exit(0);
        }
        loadLevel();
        return currentLevel;
    }

    public void loadLevel(){
//        Load Map From list
//        Set Properties
//        Set SpawnLocation
//        Set Portal if any
//        Set Monsters
//        Set Items

        LevelGenerator level = lstMaps.get(currentLevel);
        level.buildLevel();
        this.currentLocation = level.getPlayerSpawnPoint();
        this.level = level;
    }

    public void move(MoveDirection direction){
//        FIXME: Change from Switch to IF
        if (direction == MoveDirection.LEFT){
            moveLeft();
        } else if (direction == MoveDirection.RIGHT){
            moveRight();
        } else if (direction == MoveDirection.UP){
            moveUp();
        } else if (direction == MoveDirection.DOWN){
            moveDown();
        }

        Room currentRoom = this.lstMaps.get(currentLevel).getRoom(this.currentLocation);

        if (currentRoom.isSpecialRoom() && currentRoom.getSpecialRoomType() == SpecialRoom.PortalDoor){
            System.out.println("Hana: You found a magical doorway!");
            goToNextLevel();
        }
    }

    private void moveLeft(){
        if (this.currentLocation.getX() > 0){
            this.currentLocation.translate(-1, 0);
        }else {
            hitAWall();
        }
    }

    private void moveRight(){
        if (this.currentLocation.getX() < this.lstMaps.get(currentLevel).getMapWidth()-1){
            this.currentLocation.translate(1, 0);
        }else {
            hitAWall();
        }

    }

    private void moveUp(){
        if (this.currentLocation.getY() > 0){
            this.currentLocation.translate(0, -1);
        } else {
            hitAWall();
        }
    }

    private void moveDown(){
        if (this.currentLocation.getY() < this.lstMaps.get(currentLevel).getHeight() - 1){
            this.currentLocation.translate(0, 1);
        } else {
            hitAWall();
        }
    }
    private void hitAWall(){
        System.out.println("Hana: You hit a wall!");
    }

    public void printMapDetails(){
        for (int i = 0; i < lstMaps.get(currentLevel).getHeight(); i++) for (int j = 0; j < lstMaps.get(currentLevel).getMapWidth(); j++){
            System.out.println("I: " + i + " J: " + j);
            System.out.println(lstMaps.get(currentLevel).buildLevel()[i][j].containItem());
            if (lstMaps.get(currentLevel).buildLevel()[i][j].containItem())
                System.out.println(lstMaps.get(currentLevel).buildLevel()[i][j].getItem().getName());
        }
    }

    public Player getPlayer(){
        return player;
    }

    public Room getCurrentRoom(){
        return lstMaps.get(currentLevel).getRoom(currentLocation);
    }


}
