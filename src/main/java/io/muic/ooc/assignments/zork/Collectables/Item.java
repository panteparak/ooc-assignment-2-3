package io.muic.ooc.assignments.zork.Collectables;

/**
 * Created by panteparak on 1/31/17.
 */
public class Item {
    private String name;

    public Item(String itemName) {
        this.name = itemName;
    }

    public String getName() {
        return name;
    }
}
