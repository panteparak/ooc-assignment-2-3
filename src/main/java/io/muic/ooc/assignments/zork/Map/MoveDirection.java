package io.muic.ooc.assignments.zork.Map;

/**
 * Created by panteparak on 1/30/17.
 */
public enum  MoveDirection {
    UP, DOWN, LEFT, RIGHT
}
