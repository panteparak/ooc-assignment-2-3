package io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon;

import io.muic.ooc.assignments.zork.Collectables.AttackWeapon;

/**
 * Created by panteparak on 1/31/17.
 */
public class Sword extends AttackWeapon {
    /*
    TODO: Custom weapon stat randomization
     */

    public Sword() {
        super("Sword",0.8, 100);
    }


//    @Override
//    protected double randomizeWeaponAccuracy() {
//        return 0;
//    }
//
//    @Override
//    protected int randomizeAttackPower() {
//        return 0;
//    }
}
