package io.muic.ooc.assignments.zork.Map.LevelGenerator;

import io.muic.ooc.assignments.zork.Collectables.CollectablesFactory;
import io.muic.ooc.assignments.zork.Collectables.CollectibleItem;
import io.muic.ooc.assignments.zork.Exceptions.InvalidLevelConfigurationException;
import io.muic.ooc.assignments.zork.Collectables.Item;
import io.muic.ooc.assignments.zork.Map.Room;
import io.muic.ooc.assignments.zork.Map.SpecialRoom;
import io.muic.ooc.assignments.zork.Characters.Character;

import java.awt.*;
import java.util.Random;

public class LevelGenerator{
    private LevelConfigurator levelConfigurator = null;
    private Random random = new Random();
    private Room[][] rooms = null;

    public LevelGenerator(LevelConfigurator levelConfigurator) {
        validateConfig(levelConfigurator);
        this.levelConfigurator = levelConfigurator;
    }

    private void validateConfig(LevelConfigurator levelConfigurator){
        if (levelConfigurator.setItemSpawnProbability() > 1 || levelConfigurator.setItemSpawnProbability() < 0)
            throw new InvalidLevelConfigurationException("Item Spawn Probability is out of bound: " + levelConfigurator.setItemSpawnProbability());

        if (levelConfigurator.mapHeight() < 1) throw new InvalidLevelConfigurationException("Map Height is invalid: " + levelConfigurator.mapHeight());
        if (levelConfigurator.mapWidth() < 1) throw new InvalidLevelConfigurationException("Map Width is invalid: " + levelConfigurator.mapWidth());
        if (levelConfigurator.setMonsterSpawnProbability() > 1 || levelConfigurator.setMonsterSpawnProbability() < 0)
            throw new InvalidLevelConfigurationException("Out of bound Monster Spawn Probability: " + levelConfigurator.setMonsterSpawnProbability());

        if (levelConfigurator.levelName() == null) throw new InvalidLevelConfigurationException("Invalid Level Name: " + "null");
        if (levelConfigurator.levelName().trim().isEmpty()) throw new InvalidLevelConfigurationException("Invalid Level Name");
        if (levelConfigurator.setPortalPoint() == null || (levelConfigurator.setPortalPoint().getX() >= levelConfigurator.mapWidth() || levelConfigurator.setPortalPoint().getY() >= levelConfigurator.mapHeight()))
            throw new InvalidLevelConfigurationException("Invalid Portal Door Coordinates");

        if (levelConfigurator.setPlayerSpawnPoint() == null || (levelConfigurator.setPlayerSpawnPoint().getX() >= levelConfigurator.mapWidth() || levelConfigurator.setPlayerSpawnPoint().getY() >= levelConfigurator.mapHeight()))
            throw new InvalidLevelConfigurationException("Invalid Player Spawn Coordinates");
    }

    private Room[][] generateRooms(){
        Room[][] arrRooms = new Room[levelConfigurator.mapWidth()][levelConfigurator.mapHeight()];
//        System.out.println("MapSize: " + levelConfigurator.mapWidth() + ", " + levelConfigurator.mapHeight());

        for (int i = 0; i < levelConfigurator.mapWidth(); i++) for (int j = 0; j <  levelConfigurator.mapHeight(); j++){
            arrRooms[i][j] = new Room();
            if (isCurrentPoint(i, j, levelConfigurator.setPlayerSpawnPoint())){
                arrRooms[i][j].setSpecialRoom(SpecialRoom.SpawnPoint);
            } else if (isCurrentPoint(i, j, levelConfigurator.setPortalPoint())){
                arrRooms[i][j].setSpecialRoom(SpecialRoom.PortalDoor);
            }else {
                arrRooms[i][j].setItem(spawnItem());
            }
        }
        return arrRooms;
    }

    private boolean isCurrentPoint(int col, int row, Point point){
        return col == point.getY() && row == point.getX();
    }

    private Item spawnItem(){
        int randNum = this.random.nextInt(100);
        if (randNum < 15) return null;
        else if (randNum >= 15 && randNum < 40) return randomItem();
        else return randomMonster();
        
    }

    public Room getRoom(int col, int row){
        return buildLevel()[col][row];
    }

    public Room getRoom(Point point){
        return buildLevel()[point.y][point.x];
    }

    public Point getPlayerSpawnPoint(){
        return levelConfigurator.setPlayerSpawnPoint();
    }

    public Point getPortalDoorPoint(){
        return levelConfigurator.setPortalPoint();
    }

    public String getLevelName(){
        return levelConfigurator.levelName();
    }

    private CollectibleItem randomItem(){

//        If MonsterSpawn is larger than map size

        double prob = Math.abs(1 - this.levelConfigurator.setItemSpawnProbability());
        if (this.random.nextDouble() >= prob){
            return CollectablesFactory.randomItem();
        }

        return null;
    }

    private Character randomMonster(){
        double prob = Math.abs(1 - this.levelConfigurator.setMonsterSpawnProbability());
        double n = this.random.nextDouble();
        if (n >= prob){
            return CollectablesFactory.randomMonster();
        }
        return null;
    }

    public Room[][] buildLevel(){
        if (this.rooms == null){
            this.rooms = generateRooms();
        }
        return this.rooms;
    }

    public int getMapWidth(){
        return levelConfigurator.mapWidth();
    }

    public int getHeight(){
        return levelConfigurator.mapHeight();
    }
}
