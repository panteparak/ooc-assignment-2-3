package io.muic.ooc.assignments.zork.Exceptions;

/**
 * Created by panteparak on 2/3/17.
 */
public class InvalidLevelConfigurationException extends RuntimeException {

    public InvalidLevelConfigurationException() {
    }

    public InvalidLevelConfigurationException(String message) {
        super(message);
    }
}
