package io.muic.ooc.assignments.zork.Characters;

import io.muic.ooc.assignments.zork.Collectables.Item;

/**
 * Created by panteparak on 1/31/17.
 */
public class Character extends Item {
    public Character(String itemName) {
        super(itemName);
    }

    private int maxAttack = 0;
    private int maxDefence = 0;
    private double attackChance = 0;
    private double defenceChance = 0;

    public int getMaxAttack() {
        return maxAttack;
    }

    public void setMaxAttack(int maxAttack) {
        this.maxAttack = maxAttack;
    }

    public int getMaxDefence() {
        return maxDefence;
    }

    public void setMaxDefence(int maxDefence) {
        this.maxDefence = maxDefence;
    }

    public double getAttackChance() {
        return attackChance;
    }

    public void setAttackChance(double attackChance) {
        this.attackChance = attackChance;
    }

    public double getDefenceChance() {
        return defenceChance;
    }

    public void setDefenceChance(double defenceChance) {
        this.defenceChance = defenceChance;
    }
}
