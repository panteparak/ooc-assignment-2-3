package io.muic.ooc.assignments.zork;

/**
 * Created by panteparak on 1/25/17.
 */
public class Main {
    public static void main(String[] args) {
        GameController gameController = new GameController();
        gameController.start();
    }
}
