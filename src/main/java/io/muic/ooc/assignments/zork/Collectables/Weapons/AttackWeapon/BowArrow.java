package io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon;

import io.muic.ooc.assignments.zork.Collectables.AttackWeapon;

/**
 * Created by panteparak on 1/31/17.
 */
public class BowArrow extends AttackWeapon {

    public BowArrow() {
        super("BowArrow",0.7, 150);
    }

//    @Override
//    protected double randomizeWeaponAccuracy() {
//        return 0;
//    }
//
//    @Override
//    protected int randomizeAttackPower() {
//        return 0;
//    }
}
