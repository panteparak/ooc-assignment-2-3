package io.muic.ooc.assignments.zork.Map;

/**
 * Created by panteparak on 2/3/17.
 */
public enum  SpecialRoom {
    SpawnPoint, PortalDoor
}
