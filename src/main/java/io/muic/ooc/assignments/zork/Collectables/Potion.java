package io.muic.ooc.assignments.zork.Collectables;

/**
 * Created by panteparak on 1/31/17.
 */
public class Potion extends CollectibleItem {
    public Potion(String itemName) {
        super(itemName);
    }

    private boolean used = false;

    public boolean isUsed(){
        return used;
    }

    public void togglePotionUsed() {
        this.used = true;
    }

    public int healPoint = 20;

    public int getHealPoint() {
        return healPoint;
    }

    public void setHealPoint(int healPoint) {
        this.healPoint = healPoint;
    }

    //    TODO: Potion effects includes health increase or strengths
//    protected abstract void randomizePotionEffect();
}
