package io.muic.ooc.assignments.zork.Characters;


import io.muic.ooc.assignments.zork.Collectables.AttackWeapon;
import io.muic.ooc.assignments.zork.Collectables.CollectibleItem;
import io.muic.ooc.assignments.zork.Collectables.Item;
import io.muic.ooc.assignments.zork.Collectables.Potion;
import io.muic.ooc.assignments.zork.Map.Map;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Player{

    private int healthPoint = 100;
    private List<AttackWeapon> attackWeaponList;
    private List<Potion> potionList;
    private Random random = new Random();

    public Player() {
//        super("Player");
        this.attackWeaponList = new ArrayList<>();
        this.potionList = new ArrayList<>();
    }


    public int getHealthPoint() {
        return healthPoint;
    }

    public void increaseHealthPoint(int n){
        this.healthPoint += n;
    }
    public void decreaseHealthPoint(int n){
        this.healthPoint -= n;
    }

    private boolean addWeapon(AttackWeapon attackWeapon){
        if (attackWeapon != null && this.attackWeaponList.size() < 3){
            this.attackWeaponList.add(attackWeapon);
            return true;
        }
        return false;
    }

    private boolean addPotion(Potion potion){
        if (potion != null && this.potionList.size() < 3){
            this.potionList.add(potion);
            return true;
        }
        return false;
    }

    public void addItem(Item item){
        if (item == null) return;
        if (item instanceof AttackWeapon){
            addWeapon((AttackWeapon) item);

        }else if (item instanceof Potion){
            addPotion((Potion) item);
        }
    }

    public CollectibleItem removeItem(String str){
        for (AttackWeapon weapon : getAttackWeaponList()){
            if (weapon.getName().equalsIgnoreCase(str)){
                AttackWeapon toBeRemove  = weapon;
                getAttackWeaponList().remove(weapon);

                return toBeRemove;
            }
        }

        for (Potion potion : getPotionList()){
            if (potion.getName().equalsIgnoreCase(str)){
                Potion toBeRemove  = potion;
                getAttackWeaponList().remove(potion);
                return toBeRemove;
            }
        }
        System.out.println("You're not carrying this item!");
        return null;

    }

    public List<AttackWeapon> getAttackWeaponList() {
        return attackWeaponList;
    }

    public List<Potion> getPotionList() {
        return potionList;
    }

    private boolean isCarryingWeapon(String weapon){
        for (AttackWeapon attackWeapon : getAttackWeaponList()){
            if (attackWeapon.getName().equalsIgnoreCase(weapon)) return true;
        }
        return false;

//        return getAttackWeaponList().stream()
//                .filter((predicate) -> predicate.getName().intern().equalsIgnoreCase(weapon.intern().trim()))
//                .count() > 1;
    }

    private boolean isCarryingPotion(String strPotion){
        for (Potion potion : getPotionList()){
            if (potion.getName().equalsIgnoreCase(strPotion)) return true;
        }
        return false;
    }

    public boolean isCarrying(String item){
        return isCarryingWeapon(item) || isCarryingPotion(item);
    }

    private AttackWeapon getAttackWeapon(String weaponName){
        for (AttackWeapon attackWeapon : getAttackWeaponList()){
            if (attackWeapon.getName().equalsIgnoreCase(weaponName.trim()))
                return attackWeapon;
        }
        return null;

    }

    public boolean attack(Map map, String weapon){
        if (map.getCurrentRoom().getItem() instanceof MonsterNPC){
            MonsterNPC monster =  (MonsterNPC) map.getCurrentRoom().getItem();
            Player player = map.getPlayer();

            if (player.isCarryingWeapon(weapon)){
                AttackWeapon attackWeapon = player.getAttackWeapon(weapon);
                if (attackWeapon.getFullAttackPower() > monster.getMaxDefence()){


                    map.getPlayer().decreaseHealthPoint((int)(monster.getMaxAttack() * monster.getAttackChance() / 2));
                    map.getCurrentRoom().removeItem();
                }else {
                    if (random.nextDouble() >= monster.getAttackChance()){
                        map.getPlayer().decreaseHealthPoint((int) Math.abs(monster.getMaxDefence() - attackWeapon.getFullAttackPower()) / 2);
                    }
                }
            } else {
                System.out.println("You don't have that weapon");
            }

            return true;
        }
        return false;
    }

    public boolean isAlive(){
        return this.healthPoint > 0;
    }

    public boolean weaponBeltAvailable(){
        return this.getAttackWeaponList().size() < 4;
    }

    public boolean pouchAvailable(){
        return this.getPotionList().size() < 4;
    }
}
