package io.muic.ooc.assignments.zork.Collectables;

import io.muic.ooc.assignments.zork.Collectables.Potions.Medicine;
import io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon.BowArrow;
import io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon.Sword;
import io.muic.ooc.assignments.zork.Characters.MonsterNPC;
import io.muic.ooc.assignments.zork.Characters.Monsters.Sheri;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by panteparak on 2/3/17.
 */
public class CollectablesFactory {

    private static Random random = null;
    private static List<String> lstCollectables = new ArrayList<>(Arrays.asList("BowArrow", "Sword", "Medicine"));

    public static CollectibleItem randomItem(){
       int n = getRandom().nextInt(lstCollectables.size());
       switch (lstCollectables.get(n)){
           case "BowArrow": return AttackWeapon.createBowArrow();
           case "Sword": return AttackWeapon.createSword();
           case "Medicine": return Potion.createMedicine();
           default: return null;
       }
    }

    public static MonsterNPC randomMonster(){
        return new Sheri();
    }

    private static Random getRandom() {
        if (random == null){
            random = new Random();
        }
        return random;
    }

    public static class AttackWeapon{
        public static BowArrow createBowArrow(){
            return new BowArrow();
        }

        public static Sword createSword(){
            return new Sword();
        }

    }

    public static class Potion{
        public static Medicine createMedicine(){
            return new Medicine();
        }

    }

    public static List<String> getLstCollectables() {
        return lstCollectables;
    }
}
