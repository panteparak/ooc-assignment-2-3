package io.muic.ooc.assignments.zork.Map.LevelGenerator;

import java.awt.*;

/**
 * Created by panteparak on 1/31/17.
 */
public interface LevelConfigurator {
//    boolean setAdvanceLevel();

    String levelName();
    double setMonsterSpawnProbability();
    double setItemSpawnProbability();
    int mapWidth();
    int mapHeight();
    Point setPlayerSpawnPoint();
    Point setPortalPoint();
}
