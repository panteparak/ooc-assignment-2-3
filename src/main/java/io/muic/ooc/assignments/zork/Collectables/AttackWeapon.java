package io.muic.ooc.assignments.zork.Collectables;

/**
 * Created by panteparak on 1/31/17.
 */
public class AttackWeapon extends CollectibleItem {
//    Should Be a Chance of Full Atack;
//    public abstract double attackChance();
//    public abstract int setFullAttack();


    public AttackWeapon(String itemName, double attackAccuracy, int attackPower) {
        super(itemName);

        this.fullAttackChance = attackAccuracy;
        this.fullAttackPower = attackPower;
    }

    private double fullAttackChance;
    private int fullAttackPower;

    public double getFullAttackChance() {
        return fullAttackChance;
    }

    public void setFullAttackChance(double fullAttackChance) {
        if (fullAttackChance >= 0 && fullAttackChance <= 1){

            this.fullAttackChance = fullAttackChance;
        }
    }

    public int getFullAttackPower() {
        return fullAttackPower;
    }

    public void setFullAttackPower(int fullAttackPower) {
        this.fullAttackPower = fullAttackPower;
    }

//    protected abstract double randomizeWeaponAccuracy();
//    protected abstract int randomizeAttackPower();
}
