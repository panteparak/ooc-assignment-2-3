package io.muic.ooc.assignments.zork.Characters.Friendly;

import io.muic.ooc.assignments.zork.Characters.FriendlyNPC;

/**
 * Created by panteparak on 2/3/17.
 */
public class Hana extends FriendlyNPC {
    public Hana(String itemName) {
        super(itemName);
    }
}
