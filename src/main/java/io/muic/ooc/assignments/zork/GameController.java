package io.muic.ooc.assignments.zork;

import io.muic.ooc.assignments.zork.CommandLineParser.CmdParser;
import io.muic.ooc.assignments.zork.Map.LevelGenerator.LevelConfigurator;
import io.muic.ooc.assignments.zork.Map.LevelGenerator.LevelGenerator;
import io.muic.ooc.assignments.zork.Map.Map;

import java.awt.*;

public class GameController {
    Map map = null;

    public GameController() {
        this.map = mapConfig();
    }

    public void start(){
        CmdParser cmdParser = new CmdParser(this.map);
        cmdParser.start();
    }

    private Map mapConfig(){
        Map map = new Map();
//        Max Monster Has no effect
//        map.addLevel(levelOne());
//        map.addLevel(levelTwo());
//        map.addLevel(levelThree());
        map.addAllLevels(levelOne(), levelTwo(), levelThree());
        map.loadLevel();
        return map;
    }

    private LevelGenerator levelOne(){
        LevelGenerator level = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Level 1";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0.4;
            }


            @Override
            public double setItemSpawnProbability() {
                return 0.99;
            }

            @Override
            public int mapWidth() {
                return 5;
            }

            @Override
            public int mapHeight() {
                return 5;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(0,0);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(4,4);
            }
        });
        return level;
    }

    private LevelGenerator levelTwo(){
        return new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Level 2";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0.8;
            }

            @Override
            public double setItemSpawnProbability() {
                return 0.3;
            }

            @Override
            public int mapWidth() {
                return 4;
            }

            @Override
            public int mapHeight() {
                return 2;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(2,1);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(0,1);
            }
        });
    }


    private LevelGenerator levelThree(){
        return new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Level 3";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0.9;
            }


            @Override
            public double setItemSpawnProbability() {
                return 0.3;
            }

            @Override
            public int mapWidth() {
                return 3;
            }

            @Override
            public int mapHeight() {
                return 3;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(2,1);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(0,0);
            }
        });
    }
}
