package io.muic.ooc.assignments.zork.Characters;

/**
 * Created by panteparak on 1/31/17.
 */
public class MonsterNPC extends Character {
    public MonsterNPC(String itemName, int maxAttack, double attackChance, int maxDefence, double defenceChance) {
        super(itemName);

        super.setMaxAttack(maxAttack);
        super.setAttackChance(attackChance);
        super.setMaxDefence(maxDefence);
        super.setDefenceChance(defenceChance);
    }

    private int health = 50;

    public void setHealth(int n){
        this.health = n;
    }

    public void decreaseHealth(int n){
        if (health > 0){
            this.health -= n;
        }
    }

//    public void
}
