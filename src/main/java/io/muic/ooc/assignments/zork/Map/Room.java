package io.muic.ooc.assignments.zork.Map;

import io.muic.ooc.assignments.zork.Collectables.Item;

/**
 * Created by panteparak on 1/30/17.
 */
public class Room {

    private Item item = null;
    private boolean containItem = false;
    private boolean specialRoom = false;
    private SpecialRoom specialRoomType = null;


    public SpecialRoom getSpecialRoomType() {
        return specialRoomType;
    }

    public boolean isSpecialRoom() {
        return specialRoom;
    }

    public void setSpecialRoom(SpecialRoom specialRoom) {
        this.specialRoom = true;
        this.specialRoomType = specialRoom;

    }

    public Item getAndRemoveItem(){
        Item item = this.item;
        removeItem();
        return item;
    }

    public Item getItem(){
        return this.item;
    }

    public void setItem(Item item){
        if ((!containItem && item != null) && !isSpecialRoom()){
            this.item = item;
            containItem = true;
        }
    }

    public void removeItem(){
        item = null;
        containItem = false;
    }

    public boolean containItem() {
        return containItem;
    }
}
