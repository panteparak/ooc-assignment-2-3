package io.muic.ooc.assignments.zork.CommandLineParser;

import io.muic.ooc.assignments.zork.Collectables.AttackWeapon;
import io.muic.ooc.assignments.zork.Collectables.CollectibleItem;
import io.muic.ooc.assignments.zork.Collectables.Potion;
import io.muic.ooc.assignments.zork.Map.Map;
import io.muic.ooc.assignments.zork.Map.MoveDirection;
import io.muic.ooc.assignments.zork.Map.SpecialRoom;
import io.muic.ooc.assignments.zork.Characters.MonsterNPC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class CmdParser {
    Scanner scanner;
    boolean quit = false;
    Map map = null;

    private static class Command {
        static final String INFO = "info";
        static final String TAKE_ITEM = "take";
        static final String DROP_ITEM = "drop";
        static final String USE_POTION = "use";
        static final String GO_DIRECTION = "go";
        static final String ATTACK_WITH_WEAPON = "attack-with";
        static final String HELP = "help";
        static final String QUIT = "quit";
        static final String LOOK = "look";
    }

    public CmdParser(Map map) {
        this.map = map;
    }

    public void start(){
        welcomeMessage();
        scanner = new Scanner(System.in);
        String input;
        while (!quit){
            input = scanner.nextLine().trim().toLowerCase();
            if (!input.isEmpty()) parser(input);
        }
        System.exit(1);
    }

    private String[] sliceString(String line){
        return line.split(" ", 2);
    }

    private void parser(String line){
        if (Command.INFO.equalsIgnoreCase(line))
            getPlayerInfo();

        else if (line.contains(Command.TAKE_ITEM))

            takeItem();

        else if (line.contains(Command.DROP_ITEM))
            dropItem(sliceString(line));

        else if (line.contains(Command.USE_POTION))
            usePotion(sliceString(line));

        else if (line.contains(Command.GO_DIRECTION))
            goDirection(sliceString(line));

        else if (line.contains(Command.ATTACK_WITH_WEAPON))
            attack(sliceString(line));

        else if (Command.HELP.equalsIgnoreCase(line))
            help();

        else if (Command.QUIT.equalsIgnoreCase(line))
            quit();

        else if (Command.LOOK.equalsIgnoreCase(line))
            look();
        else {
            System.out.println("Hana: I don't know that command");
        }

    }

    private void welcomeMessage(){
        System.out.println("Hana: Welcome to Demon World");
        System.out.println("Hana: I am Hana, your follower");
        System.out.println("Hana: type 'help' to print out game instructions");
        System.out.println("Hana: or 'look' to see what's in the room");
    }

    public void getPlayerInfo(){
        StringBuilder str = new StringBuilder();
        final String NEWLINE = System.getProperty("line.separator");

        str.append("Level: ")
                .append(map.getLevel().getLevelName())
                .append(NEWLINE)
                .append("Current Location: ")
                .append(map.getCurrentLocation())
                .append(NEWLINE)
                .append(NEWLINE)
                .append("Player Info")
                .append(NEWLINE)
                .append("Health: ")
                .append(map.getPlayer().getHealthPoint())
                .append(NEWLINE)
                .append("Weapons:").append(NEWLINE);

        if (map.getPlayer().getAttackWeaponList().size() == 0) str.append("Player don't carry any weapon");

        else{
            for (AttackWeapon weapon : map.getPlayer().getAttackWeaponList()){
                str.append(weapon.getName()).append(" | ATK - ").append(weapon.getFullAttackPower()).append(",");
            }

            str.substring(0, str.lastIndexOf(","));
        }
        str.append(NEWLINE).append(NEWLINE).append("Potion: ");

        if (map.getPlayer().getPotionList().size() == 0) str.append(NEWLINE).append("Player don't carry any potion");
        else{

            for (Potion potion : map.getPlayer().getPotionList()){
                str.append(potion.getName()).append(" | Heal - ").append(potion.getHealPoint()).append(",");
            }
            str.substring(0, str.lastIndexOf(","));
        }


        System.out.println(str);

    }

    public void takeItem(){
        if (map.getCurrentRoom().getItem() instanceof AttackWeapon && !map.getPlayer().weaponBeltAvailable()){
            System.out.println("Hana: Weapon Belt Full");
        } else if (map.getCurrentRoom().getItem() instanceof Potion && !map.getPlayer().pouchAvailable()){
            System.out.println("Hana: Pouch Full");
        }else {
            if (map.getCurrentRoom().getItem() != null && map.getCurrentRoom().getItem() instanceof CollectibleItem){
                map.getPlayer().addItem(map.getCurrentRoom().getAndRemoveItem());
                System.out.println("Hana: Item Picked Up");
            }else {
                System.out.println("Hana: No item in this room");
            }
        }
    }

    public void dropItem(String[] itemToDrop){
        if (itemToDrop.length <= 1) {
            System.out.println("Hana: You didn't say which item to drop");
            return;
        }

        if (map.getCurrentRoom().getItem() != null || map.getCurrentRoom().isSpecialRoom()) {
            System.out.println("Hana: Cannot drop item here");
            return;
        }

        String itemStr = itemToDrop[1];

        if (map.getPlayer().isCarrying(itemStr)){
            CollectibleItem removeItem = map.getPlayer().removeItem(itemStr);
            if (removeItem!= null){
                map.getCurrentRoom().setItem(removeItem);
            }
        } else {
            System.out.println("Hana: You're not carrying this item!");
        }
    }

    public void usePotion(String[] potionToUse){
        if (potionToUse.length <= 1) {
            System.out.println("Hana: You didn't say which potion to use");
            return;
        }

        if (map.getPlayer().isCarrying(potionToUse[1])){
            int healPoint = map.getPlayer().getPotionList().remove(0).getHealPoint();
            map.getPlayer().increaseHealthPoint(healPoint);
        }

    }

    public void attack(String[] weaponToUse){
        if (weaponToUse.length <= 1 || !map.getPlayer().isCarrying(weaponToUse[1])) {
            System.out.println("Hana: You choose a weapon you don't have");
            return;
        }

        boolean didAttack = map.getPlayer().attack(map, weaponToUse[1]);
        if (didAttack){
            if (!map.getPlayer().isAlive()){
                quit = true;
                System.out.println("Hana: You Died..........");
            }
        }
    }

    public void goDirection(String[] direction) {

        ArrayList<String> moveDir = new ArrayList<>(Arrays.asList("left", "right", "up", "down"));
        if  (direction.length <= 1 || !moveDir.contains(direction[1].toLowerCase().trim())){
            System.out.println("Hana: Move command not valid");
            return;
        }

        String dir = direction[1].trim();


        if (dir.equalsIgnoreCase("up")){
            map.move(MoveDirection.UP);
        } else if (dir.equalsIgnoreCase("down")){
            map.move(MoveDirection.DOWN);

        } else if (dir.equalsIgnoreCase("left")){
            map.move(MoveDirection.LEFT);

        } else if (dir.equalsIgnoreCase("right")){
            map.move(MoveDirection.RIGHT);
        } else {

        }

    }

    public void help(){
        String NEWLINE = System.getProperty("line.separator");
        StringBuilder str = new StringBuilder();
        str.append("List of Commands:").append(NEWLINE)
                .append("info - list player infos").append(NEWLINE)
                .append("take - pick up item in the current room").append(NEWLINE)
                .append("drop {ItemName} - drop player's item in the current room (if it's an empty room)").append(NEWLINE)
                .append("look - prints out what's in the current room").append(NEWLINE)
                .append("use {PotionName} - Use selected potion").append(NEWLINE)
                .append("go {Direction} - go the the the specify direction").append(NEWLINE)
                .append("attack-with {weapon} - attack monster with weapon").append(NEWLINE)
                .append("help - prints out help").append(NEWLINE)
                .append("quit - Quit the game").append(NEWLINE);


        System.out.println(str);
    }

    public void look(){

        if (map.getCurrentRoom().isSpecialRoom() && map.getCurrentRoom().getSpecialRoomType() == SpecialRoom.SpawnPoint){
            System.out.println("Hana: This is a magical place");
        }else if (map.getCurrentRoom().isSpecialRoom() && map.getCurrentRoom().getSpecialRoomType() == SpecialRoom.PortalDoor){
            System.out.println("Hana: This is a special room!");
        } else if (map.getCurrentRoom().getItem() == null){
            System.out.println("Hana: This is an empty Room");
        } else if (map.getCurrentRoom().getItem() instanceof MonsterNPC){
            System.out.println("Hana: Monsterrrrrrrrrrrrrrrr");
        } else if (map.getCurrentRoom().getItem() instanceof CollectibleItem){
            System.out.println("Hana: The worthy shall pick up the item.");
        }
    }

    public void quit(){
        System.out.println("Quiting Game....");
        quit = true;
    }
}
