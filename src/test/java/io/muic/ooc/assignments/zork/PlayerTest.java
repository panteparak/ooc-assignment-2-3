package io.muic.ooc.assignments.zork;

import io.muic.ooc.assignments.zork.Characters.Player;
import io.muic.ooc.assignments.zork.Collectables.CollectablesFactory;
import io.muic.ooc.assignments.zork.Collectables.Item;
import org.junit.Test;
import static org.junit.Assert.*;

public class PlayerTest {
    @Test
    public void addItem() throws Exception {
        Player player = new Player();
        Item item = CollectablesFactory.AttackWeapon.createBowArrow();
        player.addItem(item);
        assertTrue(player.getAttackWeaponList().contains(item));
    }

    @Test
    public void isCarryingSword() throws Exception {
        Player player = new Player();
        Item item = CollectablesFactory.AttackWeapon.createSword();
        player.addItem(item);
        assertTrue(player.isCarrying("Sword"));
    }

    @Test
    public void isCarryingBowArrow() throws Exception {
        Player player = new Player();
        Item item = CollectablesFactory.AttackWeapon.createBowArrow();
        player.addItem(item);
        assertTrue(player.isCarrying("BowArrow"));
    }

    @Test
    public void isCarryingHealthPotion() throws Exception {
        Player player = new Player();
        Item item = CollectablesFactory.Potion.createMedicine();
        player.addItem(item);
        assertTrue(player.isCarrying("Heal Medicine"));
    }

}