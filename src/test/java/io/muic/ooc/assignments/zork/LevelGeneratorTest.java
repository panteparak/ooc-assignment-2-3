package io.muic.ooc.assignments.zork;

import io.muic.ooc.assignments.zork.Exceptions.InvalidLevelConfigurationException;
import io.muic.ooc.assignments.zork.Map.LevelGenerator.LevelConfigurator;
import io.muic.ooc.assignments.zork.Map.LevelGenerator.LevelGenerator;
import io.muic.ooc.assignments.zork.Map.Room;
import io.muic.ooc.assignments.zork.Map.SpecialRoom;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.awt.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class LevelGeneratorTest {
    @Test
    public void testMapSize() throws Exception {
        LevelGenerator level = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Test";
            }


            @Override
            public double setMonsterSpawnProbability() {
                return 0;
            }

            @Override
            public double setItemSpawnProbability() {
                return 0;
            }

            @Override
            public int mapWidth() {
                return 8;
            }

            @Override
            public int mapHeight() {
                return 4;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(0,0);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(3,3);
            }
        });

        Room[][] room = level.buildLevel();

        assertTrue("Room Height Test", room.length == 8);
        for (int i = 0; i < room.length; i++) {
            assertTrue("Room Width Test", room[i].length == 4);
        }
    }

    @Test(expected = InvalidLevelConfigurationException.class)
    public void testInvalidConfig() throws Exception {
        LevelGenerator level1 = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0.8;
            }

            @Override
            public double setItemSpawnProbability() {
                return 0.2;
            }

            @Override
            public int mapWidth() {
                return 2;
            }

            @Override
            public int mapHeight() {
                return 1;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(0,0);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(1,0);
            }
        });
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testInvalidLevelName() throws Exception {
        expectedException.expect(InvalidLevelConfigurationException.class);
        expectedException.expectMessage(CoreMatchers.containsString("Invalid Level Name"));
        LevelGenerator level = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "";
            }


            @Override
            public double setMonsterSpawnProbability() {
                return 0;
            }

            @Override
            public double setItemSpawnProbability() {
                return 0;
            }

            @Override
            public int mapWidth() {
                return 2;
            }

            @Override
            public int mapHeight() {
                return 2;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(0,0);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(0,1);
            }
        });
    }

//    @Rule
//    public ExpectedException levelNameNull = ExpectedException.none();

    @Test
    public void testNullLevelName() throws Exception {
        expectedException.expect(InvalidLevelConfigurationException.class);
        expectedException.expectMessage(CoreMatchers.containsString("Invalid Level Name: null"));

        LevelGenerator level = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return null;
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0;
            }

            @Override
            public double setItemSpawnProbability() {
                return 0;
            }

            @Override
            public int mapWidth() {
                return 3;
            }

            @Override
            public int mapHeight() {
                return 1;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(2, 0);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(0,0);
            }
        });
    }

    @Test
    public void testInvalidPlayerSpawnPoint() throws Exception {
        expectedException.expect(InvalidLevelConfigurationException.class);
        expectedException.expectMessage(CoreMatchers.containsString("Invalid Player Spawn Coordinates"));

        LevelGenerator levelGenerator = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Level Test";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0.6;
            }


            @Override
            public double setItemSpawnProbability() {
                return 0.9;
            }

            @Override
            public int mapWidth() {
                return 4;
            }

            @Override
            public int mapHeight() {
                return 1;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(3,1);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(0,0);
            }
        });
    }

    @Test
    public void testNullPortalDoorPoint() throws Exception {
        expectedException.expect(InvalidLevelConfigurationException.class);
        expectedException.expectMessage(CoreMatchers.containsString("Invalid Portal Door Coordinates"));

        LevelGenerator levelGenerator = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Level Test";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 0.6;
            }


            @Override
            public double setItemSpawnProbability() {
                return 0.3;
            }

            @Override
            public int mapWidth() {
                return 5;
            }

            @Override
            public int mapHeight() {
                return 5;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(0,0);
            }

            @Override
            public Point setPortalPoint() {
                return null;
            }
        });
    }

    @Test
    public void testNullSpawnPoint() throws Exception {
        expectedException.expect(InvalidLevelConfigurationException.class);
        expectedException.expectMessage(CoreMatchers.containsString("Invalid Player Spawn Coordinates"));

        LevelGenerator levelGenerator = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Level Test";
            }


            @Override
            public double setMonsterSpawnProbability() {
                return 0.6;
            }


            @Override
            public double setItemSpawnProbability() {
                return 0.3;
            }

            @Override
            public int mapWidth() {
                return 5;
            }

            @Override
            public int mapHeight() {
                return 5;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return null;
            }

            @Override
            public Point setPortalPoint() {
                return new Point(0,0);
            }
        });
    }

    @Test
    public void testValidPlayerSpawnPoint() throws Exception{
        LevelGenerator levelGenerator = new LevelGenerator(new LevelConfigurator() {
            @Override
            public String levelName() {
                return "Test";
            }

            @Override
            public double setMonsterSpawnProbability() {
                return 1;
            }

            @Override
            public double setItemSpawnProbability() {
                return 1;
            }

            @Override
            public int mapWidth() {
                return 5;
            }

            @Override
            public int mapHeight() {
                return 5;
            }

            @Override
            public Point setPlayerSpawnPoint() {
                return new Point(4,4);
            }

            @Override
            public Point setPortalPoint() {
                return new Point(2,3);
            }
        });
    }

    LevelGenerator lg = new LevelGenerator(new LevelConfigurator() {
        @Override
        public String levelName() {
            return "NN";
        }

        @Override
        public double setMonsterSpawnProbability() {
            return 1;
        }

        @Override
        public double setItemSpawnProbability() {
            return 1;
        }

        @Override
        public int mapWidth() {
            return 4;
        }

        @Override
        public int mapHeight() {
            return 4;
        }

        @Override
        public Point setPlayerSpawnPoint() {
            return new Point(0,0);
        }

        @Override
        public Point setPortalPoint() {
            return new Point(3,3);
        }
    });

    @Test
    public void testSpecialRoomNotNull() throws Exception {
        assertNotNull(lg.getRoom(3,3).getSpecialRoomType());

    }

    @Test
    public void testSpecialRoomType() throws Exception {
        assertTrue(lg.getRoom(3,3).getSpecialRoomType() == SpecialRoom.PortalDoor);

    }

    @Test
    public void testSpawnPointNotNull() throws Exception {
        assertNotNull(lg.getRoom(0,0).getSpecialRoomType());
    }

    @Test
    public void testSpawnPointType() throws Exception {
        assertTrue(lg.getRoom(0,0).getSpecialRoomType() == SpecialRoom.SpawnPoint);
    }

    @Test
    public void testRoomWithNoSpecialDoor() throws Exception {
        assertNull(lg.getRoom(3,1).getSpecialRoomType());
    }
}