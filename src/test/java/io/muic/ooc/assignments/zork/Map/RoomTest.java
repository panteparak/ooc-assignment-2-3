package io.muic.ooc.assignments.zork.Map;

import io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon.Sword;
import io.muic.ooc.assignments.zork.Characters.Monsters.Sheri;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by panteparak on 2/4/17.
 */
public class RoomTest {
    @Test
    public void testWeaponItem() throws Exception{
        Room room = new Room();
        Sword s = new Sword();
        room.setItem(s);
        assertTrue(room.getItem() == s);
    }

    @Test
    public void testSetMonsterItem() throws Exception{
        Room room = new Room();
        Sheri s  = new Sheri();
        room.setItem(s);
        assertTrue(room.getItem() == s);
    }

    @Test
    public void testContainItemMethod() throws Exception{
        Room room = new Room();
        Sheri s  = new Sheri();
        room.setItem(s);
        assertTrue(room.containItem());
        assertNotNull(room.getItem());
    }
    @Test
    public void testNotNullGetItemMethod() throws Exception{
        Room room = new Room();
        Sheri s  = new Sheri();
        room.setItem(s);
        assertNotNull(room.getItem());
    }

    @Test
    public void testNullRoom() throws Exception{
        Room room = new Room();
        room.setItem(null);
        assertNull(room.getItem());
        assertFalse(room.containItem());
    }

    @Test
    public void testNullRoomContainItem() throws Exception{
        Room room = new Room();
        room.setItem(null);
        assertFalse(room.containItem());
    }

}