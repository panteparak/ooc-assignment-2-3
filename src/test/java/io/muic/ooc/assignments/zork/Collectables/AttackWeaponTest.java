package io.muic.ooc.assignments.zork.Collectables;

import io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon.BowArrow;
import io.muic.ooc.assignments.zork.Collectables.Weapons.AttackWeapon.Sword;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class AttackWeaponTest {
    @Test
    public void getDefaultFullAttackChance() throws Exception {
        AttackWeapon arrow = new BowArrow();
        assertTrue(arrow.getFullAttackChance() == 0.7);

    }

    @Test
    public void setFullAttackChance() throws Exception {
        AttackWeapon arrow = new BowArrow();
        arrow.setFullAttackChance(0.9);
        assertTrue(arrow.getFullAttackChance() == 0.9);
    }

    @Test
    public void setFullAttackChanceOverOne() throws Exception {
        AttackWeapon arrow = new BowArrow();
        arrow.setFullAttackChance(0.3);
        arrow.setFullAttackChance(1.1);
        assertTrue(arrow.getFullAttackChance() == 0.3);
    }

    @Test
    public void setFullAttackPower() throws Exception {
        AttackWeapon attackWeapon = new BowArrow();
        attackWeapon.setFullAttackPower(1000);
        assertTrue(attackWeapon.getFullAttackPower() == 1000);
    }

    @Test
    public void getName() throws Exception {
        AttackWeapon attackWeapon = new Sword();
        assertTrue(attackWeapon.getName().equalsIgnoreCase("Sword"));
    }

}