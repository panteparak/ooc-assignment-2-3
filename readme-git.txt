*.iml, .idea, *.iws, *.ipr (directory) is an intellij's default project file which contains preferences and dependencies' locatioon. So it doesn't need to be commit to git.
target/ is a directory that stores compiled maven projects. it doesn't need to be commit to git
.DS_Store stores custom attributes of its containing folder[According to Wikipedia], so it doesn't need to be commit to git