There are 3 files, diagram.png and diagram2.png, diagram3.png (Shows class interactions)
The first diagram shows class diagram and inheritance of the classes together.

GridItem Class:
It will be the main "Game Grid" and contain methods that will allow objects on the grid to be move using X and Y axis.

CollectableItem Class:
It is a super class of Potion and Weapon, Both of these class has similar properety such as ItemName and Description and whether the weapon is picked up by player.

CollectableItem is a subclass of GridItem such that It has to spawn in a specific grid

Character Class:
This is a subclass of GridItem Classs as mention above, it must be able to spawn on the map at a specific axis. And SHOULD be able to move within the game map.
Character Class also has its own method that will allow the subclass (Monster, Player, Firendly).
Players and Monster both share similar methods such as their Health and attack power (without the weapon)
Player has its unique method that allows for items collections and selections.
Monster can drop item (once died) by using dropItem  method, which is unique to Monster class.
FriendlyNPC are guide to the player of where is the closest item drop

Map Class:
This is used for map generations and able to read JSON file for custom attributes for the map its self as well as other Character Properety

Factory Method includes CharacterFactory, which will be used to create different types of character, it is a static method so it can be called from any class.

The second diagram shows the basic flow of the game. As seen in the picture.
The game first need to generate the map based on the property that the user set, or a default property may be load.
It then needs to generate the map into grids and place items such as players, collectables, monsters or portal to next level on the map.
Afterwards, the game will start to accept user commands and it will check whether the next cell has any property such as iteractable character or collectable items or not, and act according to the game plot, ie kill the monster, or collect an item.
Player will be able to interact with friendly NPC and be able to guide to nearest portal, items etc.
It the player is killed the level will be restart as seen in the flow chart. 